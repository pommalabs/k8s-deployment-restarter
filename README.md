# Kubernetes Deployment Restarter

[![License: MIT][project-license-badge]][project-license]
[![Donate][paypal-donations-badge]][paypal-donations]
[![standard-readme compliant][github-standard-readme-badge]][github-standard-readme]
[![GitLab pipeline status][gitlab-pipeline-status-badge]][gitlab-pipelines]
[![Quality gate][sonar-quality-gate-badge]][sonar-website]
[![Code coverage][sonar-coverage-badge]][sonar-website]
[![Renovate enabled][renovate-badge]][renovate-website]

Web service which exposes endpoints to restart Kubernetes deployments.

Kubernetes Deployment Restarter is built with [ASP.NET Core][aspnetcore-website] and
uses official [Kubernetes client][github-kubernetes-client] to interact with its APIs.

## Table of Contents

- [Install](#install)
  - [Tags](#tags)
  - [RBAC](#rbac)
  - [Configuration](#configuration)
    - [`SECURITY_API_KEYS`](#security_api_keys)
    - [`HEALTH_CHECK_PING_TIMEOUT`](#health_check_ping_timeout)
    - [`APPINSIGHTS_CONNECTION_STRING`](#appinsights_connection_string)
  - [Logging](#logging)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
  - [Editing](#editing)
  - [Restoring dependencies](#restoring-dependencies)
  - [Starting development server](#starting-development-server)
  - [Running tests](#running-tests)
  - [Building Docker image](#building-docker-image)
- [License](#license)

## Install

Kubernetes Deployment Restarter web service is provided as a Docker image.

You can quickly start a local test instance with following command:

```bash
docker run -it --rm -p 8080:8080 container-registry.pommalabs.xyz/pommalabs/k8s-deployment-restarter:latest
```

Local test instance will be listening on port 8080 and it will accept **unauthenticated** requests.
However, starting the web service without a proper Kubernetes client configuration will not be useful,
because the web service needs that configuration in order to work.

Please check the [RBAC](#rbac) and [Configuration](#configuration) sections
to find specific information about how the web service can be properly configured.

### Tags

Following tags are available:

| Tag      | Base image                                                            |
|----------|-----------------------------------------------------------------------|
| `latest` | `container-registry.pommalabs.xyz/pommalabs/dotnet:8-aspnet-chiseled` |

Each tag can be downloaded with following command, just replace `latest` with desired tag:

```bash
docker pull container-registry.pommalabs.xyz/pommalabs/k8s-deployment-restarter:latest
```

Tags are rebuilt every week by a job scheduled on GitLab CI platform.

### RBAC

Kubernetes Deployment Restarter requires the following permissions on deployments
in order to restart every deployment available in the cluster:

- Read a single deployment, used to understand whether a deployment exists.
- Patch a single deployment, used to update the annotation `kubectl.kubernetes.io/restartedAt`.
  The update triggers the deployment restart.

Following RBAC definition can be used to configure proper permissions.
Please adjust deployment name and namespace according to your setup.

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  # "namespace" omitted since ClusterRoles are not namespaced.
  name: deployment-restarter
rules:
  - apiGroups: ["apps"]
    resources: ["deployments"]
    verbs: ["get", "patch"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: deployment-restarter
subjects:
  - kind: ServiceAccount
    name: default
    namespace: deployment-restarter
roleRef:
  kind: ClusterRole
  name: deployment-restarter
  apiGroup: rbac.authorization.k8s.io
```

### Configuration

If the `KUBECONFIG` environment variable is set, then that will be used to initialize Kubernetes client configuration.
Otherwise, Kubernetes client looks for a config file at `/home/app/.kube/config`.
If that file does not exist, it checks whether it is executing inside a cluster and will use in-cluster configuration.
Finally, if nothing else exists, Kubernetes client creates a default config with `localhost:8080` as host.

Docker image can be further configured using the following environment variables.
The same variables can also be set by mounting a `.env` file in `/opt/app/`.

#### `SECURITY_API_KEYS`

Optional API keys which should be specified in order to enforce authentication
on incoming HTTP requests. API keys might not specified for local setups, but it is
strongly advised to specify it if Kubernetes Deployment Restarter web service is exposed to the internet.

Each API key is described by three properties:

- A descriptive name (`name` field).
- The key value (`value` field).
- An optional expiration timestamp (`expiresAt` field).

If at least one API key is specified, then HTTP calls
need to include it with the `X-Api-Key` header.

Examples:

```bash
# No API key is defined (default).
SECURITY_API_KEYS='[]'

# Define one API key which never expires.
SECURITY_API_KEYS='[{"name":"My API key","value":"mySecret!"}]'

# Define two API keys which never expire.
SECURITY_API_KEYS='[{"name":"First API key","value":"mySecret1"}],[{"name":"Second API key","value":"mySecret2"}]'

# Define one API key which expires in the future.
SECURITY_API_KEYS='[{"name":"My expiring API key","value":"mySecret!","expiresAt":"2100-01-01T00:00:00Z"}]'
```

#### `HEALTH_CHECK_PING_TIMEOUT`

How many seconds should K8s API ping last before failing.
Timeout value should be greater than zero.

```bash
# Set ping timeout to 2 seconds (default).
HEALTH_CHECK_PING_TIMEOUT='2'

# Set ping timeout to 10 seconds.
HEALTH_CHECK_PING_TIMEOUT='10'
```

#### `APPINSIGHTS_CONNECTION_STRING`

If you want to send web service logs and metrics to Azure Application Insights,
then you can use this environment variable to set the connection string.

```bash
# Do not send logs and metrics to Application Insights (default).
APPINSIGHTS_CONNECTION_STRING=''

# Send logs and metrics to Application Insights, using the specified connection string.
APPINSIGHTS_CONNECTION_STRING='YOUR_CONNECTION_STRING'
```

### Logging

Web service writes log messages to the console.

If an Azure Application Insights connection string is specified using the
[`APPINSIGHTS_CONNECTION_STRING`](#appinsights_connection_string) configuration,
logs and telemetry data are also sent to that service.

## Usage

Please check OpenAPI docs on your instance (just visit the `/docs` URL).

For example, if an API key with value `my_key` has been set, the deployment
`my_deployment` in namespace `my_namespace` can be restarted with the following request:

```http
POST /api/v1/restart HTTP/1.1
Host: localhost:8080
Content-Type: application/json
X-Api-Key: my_key

{
  "namespace": "my_namespace",
  "name": "my_deployment"
}
```

## Maintainers

[@pomma89][gitlab-pomma89].

## Contributing

MRs accepted.

Small note: If editing the README, please conform to the [standard-readme][github-standard-readme] specification.

### Editing

[Visual Studio Code][vscode-website], with [Remote Containers extension][vscode-remote-containers],
is the recommended way to work on this project.

A development container has been configured with all required tools.

[Visual Studio Community][vs-website] is also supported
and an updated solution file, `k8s-deployment-restarter.sln`, has been provided.

### Restoring dependencies

When opening the development container, dependencies should be automatically restored.

Anyway, dependencies can be restored with following command:

```bash
dotnet restore
```

### Starting development server

A local development server listening on port `8080` can be started with following command:

```bash
dotnet run --project ./src/Server/Server.csproj
```

### Running tests

Tests can be run with following command:

```bash
dotnet test
```

Tests can also be run with following command, which collects coverage information:

```bash
./build.sh --target run-tests
```

### Building Docker image

Docker image can be built with following command:

```bash
docker build . -f ./src/Server/Dockerfile -t $DOCKER_TAG
```

Please replace `$DOCKER_TAG` with a valid tag (e.g. `k8s-deployment-restarter`).

## License

MIT © 2021-2024 [PommaLabs Team and Contributors][pommalabs-website]

[aspnetcore-website]: https://learn.microsoft.com/en-us/aspnet/core/
[github-kubernetes-client]: https://github.com/kubernetes-client/csharp
[github-standard-readme]: https://github.com/RichardLitt/standard-readme
[github-standard-readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[gitlab-pipeline-status-badge]: https://gitlab.com/pommalabs/k8s-deployment-restarter/badges/main/pipeline.svg?style=flat-square
[gitlab-pipelines]: https://gitlab.com/pommalabs/k8s-deployment-restarter/pipelines
[gitlab-pomma89]: https://gitlab.com/pomma89
[paypal-donations]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ELJWKEYS9QGKA
[paypal-donations-badge]: https://img.shields.io/badge/Donate-PayPal-important.svg?style=flat-square
[pommalabs-website]: https://pommalabs.xyz/
[project-license]: https://gitlab.com/pommalabs/k8s-deployment-restarter/-/blob/main/LICENSE
[project-license-badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square
[renovate-badge]: https://img.shields.io/badge/renovate-enabled-brightgreen.svg?style=flat-square
[renovate-website]: https://renovate.whitesourcesoftware.com/
[sonar-coverage-badge]: https://img.shields.io/sonar/coverage/pommalabs_k8s-deployment-restarter?server=https%3A%2F%2Fsonarcloud.io&sonarVersion=8&style=flat-square
[sonar-quality-gate-badge]: https://img.shields.io/sonar/quality_gate/pommalabs_k8s-deployment-restarter?server=https%3A%2F%2Fsonarcloud.io&sonarVersion=8&style=flat-square
[sonar-website]: https://sonarcloud.io/dashboard?id=pommalabs_k8s-deployment-restarter
[vs-website]: https://visualstudio.microsoft.com/
[vscode-remote-containers]: https://code.visualstudio.com/docs/remote/containers
[vscode-website]: https://code.visualstudio.com/
