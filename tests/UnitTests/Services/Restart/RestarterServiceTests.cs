﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Net;
using k8s;
using k8s.Autorest;
using k8s.Models;
using Microsoft.Extensions.Logging;
using NodaTime;
using PommaLabs.K8sDeploymentRestarter.Models.DataTransferObjects;
using PommaLabs.K8sDeploymentRestarter.Models.Enumerations;
using PommaLabs.K8sDeploymentRestarter.Models.Exceptions;
using PommaLabs.K8sDeploymentRestarter.Services.Restart;

namespace PommaLabs.K8sDeploymentRestarter.UnitTests.Services.Restart;

[TestFixture, Parallelizable]
internal sealed class RestarterServiceTests
{
    [Test]
    public void RestartAsync_WhenReadPermissionsAreMissing_ShouldThrowRestartExceptionWithMissingPermissionsReason()
    {
        // Arrange
        var client = A.Fake<IAppsV1Operations>();
        var logger = A.Fake<ILogger<RestartService>>();
        var clock = A.Fake<IClock>();
        var restartService = new RestartService(client, logger, clock);
        var deployment = Utils.GetFakeDeployment();

        var exception = new HttpOperationException
        {
            Response = new HttpResponseMessageWrapper(new HttpResponseMessage(HttpStatusCode.Forbidden), string.Empty)
        };

        A.CallTo(() => client.ReadNamespacedDeploymentWithHttpMessagesAsync(
                deployment.Name, deployment.Namespace, A<bool?>._,
                A<IReadOnlyDictionary<string, IReadOnlyList<string>>>._, A<CancellationToken>._))
            .Throws(exception);

        // Act & Assert
        var ex = Assert.ThrowsAsync<RestartException>(() => restartService.RestartAsync(deployment, default))!;
        AssertExceptionHasBeenEnrichedWithDeploymentProperties(ex, deployment);
        Assert.That(ex.Reason, Is.EqualTo(RestartErrorReason.MissingPermissions));
        Assert.That(ex.Message, Contains.Substring("get"));
    }

    [Test]
    public void RestartAsync_WhenDeploymentDoesNotExist_ShouldThrowRestartExceptionWithDeploymentNotFoundReason()
    {
        // Arrange
        var client = A.Fake<IAppsV1Operations>();
        var logger = A.Fake<ILogger<RestartService>>();
        var clock = A.Fake<IClock>();
        var restartService = new RestartService(client, logger, clock);
        var deployment = Utils.GetFakeDeployment();

        var exception = new HttpOperationException
        {
            Response = new HttpResponseMessageWrapper(new HttpResponseMessage(HttpStatusCode.NotFound), string.Empty)
        };

        A.CallTo(() => client.ReadNamespacedDeploymentWithHttpMessagesAsync(
                deployment.Name, deployment.Namespace, A<bool?>._,
                A<IReadOnlyDictionary<string, IReadOnlyList<string>>>._, A<CancellationToken>._))
            .Throws(exception);

        // Act & Assert
        var ex = Assert.ThrowsAsync<RestartException>(() => restartService.RestartAsync(deployment, default))!;
        AssertExceptionHasBeenEnrichedWithDeploymentProperties(ex, deployment);
        Assert.That(ex.Reason, Is.EqualTo(RestartErrorReason.DeploymentNotFound));
    }

    [Test]
    public void RestartAsync_WhenReadNamespacedDeploymentAsyncThrows_ShouldEnrichExceptionAndRethrowIt()
    {
        // Arrange
        var client = A.Fake<IAppsV1Operations>();
        var logger = A.Fake<ILogger<RestartService>>();
        var clock = A.Fake<IClock>();
        var restartService = new RestartService(client, logger, clock);
        var deployment = Utils.GetFakeDeployment();

        A.CallTo(() => client.ReadNamespacedDeploymentWithHttpMessagesAsync(
                deployment.Name, deployment.Namespace, A<bool?>._,
                A<IReadOnlyDictionary<string, IReadOnlyList<string>>>._, A<CancellationToken>._))
            .Throws<TaskCanceledException>();

        // Act & Assert
        var ex = Assert.ThrowsAsync<TaskCanceledException>(() => restartService.RestartAsync(deployment, default))!;
        AssertExceptionHasBeenEnrichedWithDeploymentProperties(ex, deployment);
    }

    [Test]
    public void RestartAsync_WhenDeploymentCannotBePatched_ShouldThrowRestartExceptionWithMissingPermissionsReason()
    {
        // Arrange
        var client = A.Fake<IAppsV1Operations>();
        var logger = A.Fake<ILogger<RestartService>>();
        var clock = A.Fake<IClock>();
        var restartService = new RestartService(client, logger, clock);
        var deployment = Utils.GetFakeDeployment();

        var exception = new HttpOperationException
        {
            Response = new HttpResponseMessageWrapper(new HttpResponseMessage(HttpStatusCode.Forbidden), string.Empty)
        };

        A.CallTo(() => client.PatchNamespacedDeploymentWithHttpMessagesAsync(
                A<V1Patch>._, deployment.Name, deployment.Namespace, A<string>._, A<string>._, A<string>._,
                A<bool?>._, A<bool?>._, A<IReadOnlyDictionary<string, IReadOnlyList<string>>>._, A<CancellationToken>._))
            .Throws(exception);

        // Act & Assert
        var ex = Assert.ThrowsAsync<RestartException>(() => restartService.RestartAsync(deployment, default))!;
        AssertExceptionHasBeenEnrichedWithDeploymentProperties(ex, deployment);
        Assert.That(ex.Reason, Is.EqualTo(RestartErrorReason.MissingPermissions));
        Assert.That(ex.Message, Contains.Substring("patch"));
    }

    [Test]
    public void RestartAsync_WhenPatchNamespacedDeploymentAsyncThrows_ShouldEnrichExceptionAndRethrowIt()
    {
        // Arrange
        var client = A.Fake<IAppsV1Operations>();
        var logger = A.Fake<ILogger<RestartService>>();
        var clock = A.Fake<IClock>();
        var restartService = new RestartService(client, logger, clock);
        var deployment = Utils.GetFakeDeployment();

        A.CallTo(() => client.PatchNamespacedDeploymentWithHttpMessagesAsync(
                A<V1Patch>._, deployment.Name, deployment.Namespace, A<string>._, A<string>._, A<string>._,
                A<bool?>._, A<bool?>._, A<IReadOnlyDictionary<string, IReadOnlyList<string>>>._, A<CancellationToken>._))
            .Throws<TaskCanceledException>();

        // Act & Assert
        var ex = Assert.ThrowsAsync<TaskCanceledException>(() => restartService.RestartAsync(deployment, default))!;
        AssertExceptionHasBeenEnrichedWithDeploymentProperties(ex, deployment);
    }

    [Test]
    public void RestartAsync_WhenDeploymentCanBePatched_ShouldNotThrow()
    {
        // Arrange
        var client = A.Fake<IAppsV1Operations>();
        var logger = A.Fake<ILogger<RestartService>>();
        var clock = A.Fake<IClock>();
        var restartService = new RestartService(client, logger, clock);
        var deployment = Utils.GetFakeDeployment();

        // Act & Assert
        Assert.DoesNotThrowAsync(() => restartService.RestartAsync(deployment, default));
    }

    private static void AssertExceptionHasBeenEnrichedWithDeploymentProperties(Exception ex, Deployment deployment)
    {
        Assert.That(ex.Data, Contains.Key("Namespace").WithValue(deployment.Namespace));
        Assert.That(ex.Data, Contains.Key("Name").WithValue(deployment.Name));
    }
}
