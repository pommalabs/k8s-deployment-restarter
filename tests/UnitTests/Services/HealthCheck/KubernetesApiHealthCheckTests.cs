﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using k8s;
using k8s.Autorest;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;
using PommaLabs.K8sDeploymentRestarter.Models.Options;
using PommaLabs.K8sDeploymentRestarter.Services.HealthCheck;

namespace PommaLabs.K8sDeploymentRestarter.UnitTests.Services.HealthCheck;

[TestFixture, Parallelizable]
internal sealed class KubernetesApiHealthCheckTests
{
    [Test]
    public async Task CheckHealth_WhenKubernetesApiRespondsSuccessfully_ShouldReturnHealthyResult()
    {
        // Arrange
        var client = A.Fake<IAppsV1Operations>();
        var healthCheckOptions = new OptionsWrapper<HealthCheckOptions>(new HealthCheckOptions());
        var healthCheck = new KubernetesApiHealthCheck(client, healthCheckOptions);
        var healthCheckContext = new HealthCheckContext();

        // Act
        var result = await healthCheck.CheckHealthAsync(healthCheckContext, default);

        // Assert
        Assert.That(result.Status, Is.EqualTo(HealthStatus.Healthy));
    }

    [Test]
    public async Task CheckHealth_WhenKubernetesApiDoesNotRespondSuccessfully_ShouldReturnUnhealthyResult()
    {
        // Arrange
        var client = A.Fake<IAppsV1Operations>();
        var healthCheckOptions = new OptionsWrapper<HealthCheckOptions>(new HealthCheckOptions());
        var healthCheck = new KubernetesApiHealthCheck(client, healthCheckOptions);
        var healthCheckContext = new HealthCheckContext();

        A.CallTo(() => client.GetAPIResourcesWithHttpMessagesAsync(
                A<IReadOnlyDictionary<string, IReadOnlyList<string>>>._, A<CancellationToken>._))
            .Throws<HttpOperationException>();

        // Act
        var result = await healthCheck.CheckHealthAsync(healthCheckContext, default);

        // Assert
        Assert.That(result.Status, Is.EqualTo(HealthStatus.Unhealthy));
        Assert.That(result.Description, Contains.Substring("error"));
        Assert.That(result.Exception, Is.Not.Null);
        Assert.That(result.Exception, Is.InstanceOf<HttpOperationException>());
    }

    [Test]
    public async Task CheckHealth_WhenKubernetesApiDoesNotRespondInTime_ShouldReturnUnhealthyResult()
    {
        // Arrange
        var client = A.Fake<IAppsV1Operations>();
        var healthCheckOptions = new OptionsWrapper<HealthCheckOptions>(new HealthCheckOptions());
        var healthCheck = new KubernetesApiHealthCheck(client, healthCheckOptions);
        var healthCheckContext = new HealthCheckContext();

        A.CallTo(() => client.GetAPIResourcesWithHttpMessagesAsync(
                A<IReadOnlyDictionary<string, IReadOnlyList<string>>>._, A<CancellationToken>._))
            .Throws<TaskCanceledException>();

        // Act
        var result = await healthCheck.CheckHealthAsync(healthCheckContext, default);

        // Assert
        Assert.That(result.Status, Is.EqualTo(HealthStatus.Unhealthy));
        Assert.That(result.Description, Contains.Substring("timeout"));
        Assert.That(result.Exception, Is.Not.Null);
        Assert.That(result.Exception, Is.InstanceOf<TaskCanceledException>());
    }
}
