﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using PommaLabs.K8sDeploymentRestarter.Models.DataTransferObjects;
using PommaLabs.K8sDeploymentRestarter.Models.Enumerations;
using PommaLabs.K8sDeploymentRestarter.Models.Exceptions;
using PommaLabs.K8sDeploymentRestarter.Server.Endpoints;
using PommaLabs.K8sDeploymentRestarter.Services.Restart;

namespace PommaLabs.K8sDeploymentRestarter.UnitTests.Server.Endpoints;

[TestFixture, Parallelizable]
internal sealed class RestartEndpointsTests
{
    [Test]
    public async Task Restart_WhenInvoked_ShouldInvokeRestartAsyncWithGivenDeployment()
    {
        // Arrange
        var deployment = Utils.GetFakeDeployment();
        var restartService = A.Fake<IRestartService>();

        // Act
        var result = await RestartEndpoints.Restart(deployment, restartService, default);

        // Assert
        A.CallTo(() => restartService.RestartAsync(deployment, A<CancellationToken>._)).MustHaveHappenedOnceExactly();
        Assert.That(result.Result, Is.InstanceOf<Ok>());
    }

    [Test]
    public async Task DeprecatedRestart_WhenInvoked_ShouldInvokeRestartAsyncWithDeploymentConstructedFromGivenParameters()
    {
        // Arrange
        var deployment = Utils.GetFakeDeployment();
        var restartService = A.Fake<IRestartService>();

        // Act
        var result = await RestartEndpoints.DeprecatedRestart(deployment.Namespace, deployment.Name, restartService, default);

        // Assert
        A.CallTo(() =>
                restartService.RestartAsync(
                    A<Deployment>.That.Matches(d => d.Namespace == deployment.Namespace && d.Name == deployment.Name),
                    A<CancellationToken>._))
            .MustHaveHappenedOnceExactly();
        Assert.That(result.Result, Is.InstanceOf<Ok>());
    }

    [Test]
    public async Task Restart_WhenRestartExceptionIsThrowWithMissingPermissionsReason_ShouldRespondUnprocessableEntityWithExceptionMessage()
    {
        // Arrange
        var deployment = Utils.GetFakeDeployment();
        var restartService = A.Fake<IRestartService>();

        var exception = new RestartException(RestartErrorReason.MissingPermissions, Utils.Faker.Lorem.Sentence());
        A.CallTo(() => restartService.RestartAsync(deployment, A<CancellationToken>._))
            .Throws(() => exception);

        // Act
        var result = await RestartEndpoints.Restart(deployment, restartService, default);

        // Assert
        Assert.That(result.Result, Is.InstanceOf<UnprocessableEntity<ProblemDetails>>());
        var notFound = (UnprocessableEntity<ProblemDetails>)result.Result;
        Assert.That(notFound.Value, Is.Not.Null);
        Assert.That(notFound.Value!.Title, Is.EqualTo(exception.Message));
        Assert.That(notFound.Value.Status, Is.EqualTo(StatusCodes.Status422UnprocessableEntity));
    }

    [Test]
    public async Task Restart_WhenRestartExceptionIsThrowWithDeploymentNotFoundReason_ShouldRespondNotFoundWithExceptionMessage()
    {
        // Arrange
        var deployment = Utils.GetFakeDeployment();
        var restartService = A.Fake<IRestartService>();

        var exception = new RestartException(RestartErrorReason.DeploymentNotFound, Utils.Faker.Lorem.Sentence());
        A.CallTo(() => restartService.RestartAsync(deployment, A<CancellationToken>._))
            .Throws(() => exception);

        // Act
        var result = await RestartEndpoints.Restart(deployment, restartService, default);

        // Assert
        Assert.That(result.Result, Is.InstanceOf<NotFound<ProblemDetails>>());
        var notFound = (NotFound<ProblemDetails>)result.Result;
        Assert.That(notFound.Value, Is.Not.Null);
        Assert.That(notFound.Value!.Title, Is.EqualTo(exception.Message));
        Assert.That(notFound.Value.Status, Is.EqualTo(StatusCodes.Status404NotFound));
    }
}
