﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Bogus;
using PommaLabs.K8sDeploymentRestarter.Models.DataTransferObjects;

namespace PommaLabs.K8sDeploymentRestarter.UnitTests;

internal static class Utils
{
    public static Faker Faker { get; } = new();

    public static Deployment GetFakeDeployment()
    {
        return new Deployment { Namespace = Faker.Lorem.Word(), Name = Faker.Lorem.Word() };
    }
}
