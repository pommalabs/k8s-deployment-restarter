﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Net;
using Microsoft.AspNetCore.Mvc.Testing;
using PommaLabs.Salatino.Endpoints;

namespace PommaLabs.K8sDeploymentRestarter.IntegrationTests.Server;

[TestFixture, Parallelizable]
internal sealed class ProgramTests
{
    [TestCase("/")]
    [TestCase("/docs")]
    [TestCase(AppInfoEndpoints.CheckAppHealthRoute)]
    [TestCase(AppInfoEndpoints.GetAppVersionRoute)]
    public async Task Program_WhenActivated_ShouldExposeCommonEndpoints(string route)
    {
        // Arrange
        await using var web = new WebApplicationFactory<Program>();
        using var client = web.CreateClient();

        // Act
        var response = await client.GetAsync(route);

        // Assert
        Assert.That(response.StatusCode, Is.LessThan(HttpStatusCode.BadRequest).Or.EqualTo(HttpStatusCode.ServiceUnavailable));
    }
}
