﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Net;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using PommaLabs.K8sDeploymentRestarter.Models.DataTransferObjects;
using PommaLabs.K8sDeploymentRestarter.Services.Restart;

namespace PommaLabs.K8sDeploymentRestarter.IntegrationTests.Server.Endpoints;

[TestFixture, Parallelizable]
internal sealed class RestartEndpointsTests
{
    [Test]
    public async Task Restart_WhenInvoked_ShouldInvokeRestartAsyncWithGivenDeployment()
    {
        // Arrange
        var restartService = A.Fake<IRestartService>();
        await using var web = new WebApplicationFactory<Program>().WithWebHostBuilder(builder =>
        {
            builder.ConfigureTestServices(services =>
            {
                services.Remove(services.Single(x => x.ServiceType == typeof(IRestartService)));
                services.AddSingleton(restartService);
            });
        });
        using var client = web.CreateClient();
        var deployment = Utils.GetFakeDeployment();

        // Act
        var response = await client.PostAsJsonAsync("/api/v1/restart", deployment);

        // Assert
        A.CallTo(() => restartService.RestartAsync(deployment, A<CancellationToken>._)).MustHaveHappenedOnceExactly();
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
    }

    [Test]
    public async Task DeprecatedRestart_WhenInvoked_ShouldInvokeRestartAsyncWithDeploymentConstructedFromGivenParameters()
    {
        // Arrange
        var restartService = A.Fake<IRestartService>();
        await using var web = new WebApplicationFactory<Program>().WithWebHostBuilder(builder =>
        {
            builder.ConfigureTestServices(services =>
            {
                services.Remove(services.Single(x => x.ServiceType == typeof(IRestartService)));
                services.AddSingleton(restartService);
            });
        });
        using var client = web.CreateClient();
        var deployment = Utils.GetFakeDeployment();

        // Act
        var encodedNamespace = WebUtility.UrlEncode(deployment.Namespace);
        var encodedName = WebUtility.UrlEncode(deployment.Name);
        var response = await client.GetAsync($"/api/v1/restart/{encodedNamespace}/{encodedName}");

        // Assert
        A.CallTo(() =>
                restartService.RestartAsync(
                    A<Deployment>.That.Matches(d => d.Namespace == deployment.Namespace && d.Name == deployment.Name),
                    A<CancellationToken>._))
            .MustHaveHappenedOnceExactly();
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
    }
}
