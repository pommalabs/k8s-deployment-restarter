# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [4] - 2024-11-19

### Changed

- Updated to .NET 9.

## [3] - 2023-12-17

### Changed

- Web service has been rewritten in .NET and ASP.NET Core. 
  Python source code has been [archived on GitLab](https://gitlab.com/pommalabs/k8s-deployment-restarter-py).
- Highly improved memory usage.

## [2] - 2022-11-20

### Added

- Added a `GET` endpoint, which can be used in scenarios where `POST` requests are harder to implement.

## [1] - 2022-11-20

### Added

- Initial release.

[4]: https://gitlab.com/pommalabs/k8s-deployment-restarter/-/compare/3...4
[3]: https://gitlab.com/pommalabs/k8s-deployment-restarter/-/tags/3
[2]: https://gitlab.com/pommalabs/k8s-deployment-restarter-py/-/compare/1...2
[1]: https://gitlab.com/pommalabs/k8s-deployment-restarter-py/-/tags/1
