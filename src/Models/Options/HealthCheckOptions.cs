﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FluentValidation;

namespace PommaLabs.K8sDeploymentRestarter.Models.Options;

public sealed class HealthCheckOptions
{
    /// <summary>
    ///   How many seconds should Kubernetes API ping last before failing.
    ///   Default value is 2 seconds, timeout value should be greater than zero.
    /// </summary>
    public int PingTimeout { get; set; } = 2;

    public sealed class Validator : AbstractValidator<HealthCheckOptions>
    {
        public Validator()
        {
            RuleFor(x => x.PingTimeout).GreaterThan(0);
        }
    }
}
