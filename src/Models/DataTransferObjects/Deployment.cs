﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.ComponentModel.DataAnnotations;
using FluentValidation;

namespace PommaLabs.K8sDeploymentRestarter.Models.DataTransferObjects;

public sealed record Deployment
{
    /// <summary>
    ///   Namespace to which the deployment belongs.
    /// </summary>
    [Required]
    public required string Namespace { get; init; }

    /// <summary>
    ///   Deployment name.
    /// </summary>
    [Required]
    public required string Name { get; init; }

    public class Validator : AbstractValidator<Deployment>
    {
        public Validator()
        {
            RuleFor(x => x.Namespace).NotEmpty();
            RuleFor(x => x.Name).NotEmpty();
        }
    }
}
