﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.K8sDeploymentRestarter.Models.Enumerations;

public enum RestartErrorReason
{
    MissingPermissions = 1,
    DeploymentNotFound = 2
}
