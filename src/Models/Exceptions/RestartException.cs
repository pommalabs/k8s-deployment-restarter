﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.K8sDeploymentRestarter.Models.Enumerations;

namespace PommaLabs.K8sDeploymentRestarter.Models.Exceptions;

public sealed class RestartException : Exception
{
    public RestartException() : base()
    {
    }

    public RestartException(string? message) : base(message)
    {
    }

    public RestartException(string? message, Exception? innerException) : base(message, innerException)
    {
    }

    public RestartException(RestartErrorReason reason, string? message, Exception? innerException = null) : this(message, innerException)
    {
        Reason = reason;
    }

    public RestartErrorReason Reason { get; }
}
