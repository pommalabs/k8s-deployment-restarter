﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using k8s;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;
using PommaLabs.K8sDeploymentRestarter.Models.Options;

namespace PommaLabs.K8sDeploymentRestarter.Services.HealthCheck;

public sealed class KubernetesApiHealthCheck(IAppsV1Operations client, IOptions<HealthCheckOptions> healthCheckOptions) : IHealthCheck
{
    public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
    {
        try
        {
            using var timeoutCts = new CancellationTokenSource();
            using var linkedCts = CancellationTokenSource.CreateLinkedTokenSource(timeoutCts.Token, cancellationToken);
            timeoutCts.CancelAfter(TimeSpan.FromSeconds(healthCheckOptions.Value.PingTimeout));
            await client.GetAPIResourcesAsync(linkedCts.Token);
            return HealthCheckResult.Healthy();
        }
        catch (TaskCanceledException ex)
        {
            return HealthCheckResult.Unhealthy("A timeout occurred while invoking Kubernetes API", exception: ex);
        }
        catch (Exception ex)
        {
            return HealthCheckResult.Unhealthy("An error occurred while invoking Kubernetes API", exception: ex);
        }
    }
}
