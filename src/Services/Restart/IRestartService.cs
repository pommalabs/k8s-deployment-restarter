﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.K8sDeploymentRestarter.Models.DataTransferObjects;

namespace PommaLabs.K8sDeploymentRestarter.Services.Restart;

public interface IRestartService
{
    Task RestartAsync(Deployment deployment, CancellationToken cancellationToken);
}
