﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Net;
using k8s;
using k8s.Autorest;
using k8s.Models;
using Microsoft.Extensions.Logging;
using NodaTime;
using PommaLabs.K8sDeploymentRestarter.Models.DataTransferObjects;
using PommaLabs.K8sDeploymentRestarter.Models.Enumerations;
using PommaLabs.K8sDeploymentRestarter.Models.Exceptions;

namespace PommaLabs.K8sDeploymentRestarter.Services.Restart;

public sealed class RestartService(IAppsV1Operations client, ILogger<RestartService> logger, IClock clock) : IRestartService
{
    public async Task RestartAsync(Deployment deployment, CancellationToken cancellationToken)
    {
        await EnsureDeploymentExistsAsync(deployment, cancellationToken);
        await PatchDeploymentAsync(deployment, cancellationToken);
    }

    private async Task EnsureDeploymentExistsAsync(Deployment deployment, CancellationToken cancellationToken)
    {
        try
        {
            await client.ReadNamespacedDeploymentAsync(deployment.Name, deployment.Namespace,
                cancellationToken: cancellationToken);
        }
        catch (HttpOperationException ex) when (ex.Response.StatusCode == HttpStatusCode.Forbidden)
        {
            const string ErrorMessage = "Service account cannot perform 'get' operation on 'apps/deployments' resource";
            logger.LogError(ex, ErrorMessage);
            var rex = new RestartException(RestartErrorReason.MissingPermissions, ErrorMessage, ex);
            EnrichExceptionDataWithDeploymentProperties(rex, deployment);
            throw rex;
        }
        catch (HttpOperationException ex) when (ex.Response.StatusCode == HttpStatusCode.NotFound)
        {
            var errorMessage = $"Deployment '{deployment.Namespace}/{deployment.Name}' does not exist";
            var rex = new RestartException(RestartErrorReason.DeploymentNotFound, errorMessage, ex);
            EnrichExceptionDataWithDeploymentProperties(rex, deployment);
            throw rex;
        }
        catch (Exception ex)
        {
            EnrichExceptionDataWithDeploymentProperties(ex, deployment);
            throw;
        }
    }

    private async Task PatchDeploymentAsync(Deployment deployment, CancellationToken cancellationToken)
    {
        try
        {
            var now = clock.GetCurrentInstant().ToString();
            var body = new
            {
                spec = new
                {
                    template = new
                    {
                        metadata = new
                        {
                            annotations = new Dictionary<string, string>
                            {
                                ["kubectl.kubernetes.io/restartedAt"] = now
                            }
                        }
                    }
                }
            };
            await client.PatchNamespacedDeploymentAsync(new V1Patch(body, V1Patch.PatchType.MergePatch),
                deployment.Name, deployment.Namespace, cancellationToken: cancellationToken);
            logger.LogInformation("Deployment '{Namespace}/{Name}' successfully patched", deployment.Namespace, deployment.Name);
        }
        catch (HttpOperationException ex) when (ex.Response.StatusCode == HttpStatusCode.Forbidden)
        {
            const string ErrorMessage = "Service account cannot perform 'patch' operation on 'apps/deployments' resource";
            var rex = new RestartException(RestartErrorReason.MissingPermissions, ErrorMessage, ex);
            EnrichExceptionDataWithDeploymentProperties(rex, deployment);
            throw rex;
        }
        catch (Exception ex)
        {
            EnrichExceptionDataWithDeploymentProperties(ex, deployment);
            throw;
        }
    }

    private static void EnrichExceptionDataWithDeploymentProperties(Exception ex, Deployment deployment)
    {
        ex.Data["Namespace"] = deployment.Namespace;
        ex.Data["Name"] = deployment.Name;
    }
}
