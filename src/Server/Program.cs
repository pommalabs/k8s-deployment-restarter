﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Diagnostics.CodeAnalysis;
using FluentValidation;
using k8s;
using PommaLabs.K8sDeploymentRestarter.Models.DataTransferObjects;
using PommaLabs.K8sDeploymentRestarter.Server.ConfigureOptions;
using PommaLabs.K8sDeploymentRestarter.Server.Endpoints;
using PommaLabs.K8sDeploymentRestarter.Services.HealthCheck;
using PommaLabs.K8sDeploymentRestarter.Services.Restart;
using PommaLabs.Salatino;
using Serilog;
using HealthCheckOptions = PommaLabs.K8sDeploymentRestarter.Models.Options.HealthCheckOptions;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;
var services = builder.Services;

configuration.MapEnvironmentVariablesToConfigurationEntries(new Dictionary<string, string>
{
    ["HEALTH_CHECK_PING_TIMEOUT"] = "HealthCheck:PingTimeout"
});

// Add services to the container.

services.AddSalatino(configuration);

services.AddAuthentication()
    .AddApiKeyAuthentication()
    .AddDummyAuthentication();

services.AddAuthorization(opts =>
{
    opts.AddPolicy(Constants.ApiKeyAuthorizationPolicy, policy =>
    {
        policy.AddAuthenticationSchemes(Constants.ApiKeyAuthenticationScheme);
        policy.RequireAuthenticatedUser();
    });

    opts.DefaultPolicy = opts.GetPolicy(Constants.ApiKeyAuthorizationPolicy)!;
});

services.AddValidatorsFromAssembly(typeof(Deployment).Assembly);

services.ConfigureOptions<ConfigureSwaggerGenOptions>();

services.AddOptionsWithValidation<HealthCheckOptions, HealthCheckOptions.Validator>();

services.AddHealthChecks()
    .AddCheck<KubernetesApiHealthCheck>("Kubernetes API");

services.AddSingleton(KubernetesClientConfiguration.BuildDefaultConfig());
services.AddSingleton(ctx => new Kubernetes(ctx.GetRequiredService<KubernetesClientConfiguration>()));
services.AddSingleton(ctx => ctx.GetRequiredService<Kubernetes>().AppsV1);

services.AddSingleton<IRestartService, RestartService>();

builder.WebHost.DisableServerHeader().UsePortEnvironmentVariable();

var app = builder.Build();

// Configure the HTTP request pipeline.

using (app.UseSalatino("Kubernetes Deployment Restarter"))
{
    app.MapRedirectsToOpenApiDocs();
    app.MapAppInfoEndpoints();

    var restartGroup = app.MapGroup("/api/v1/restart")
        .RequireAuthorization()
        .WithFluentValidation()
        .WithTags("DeploymentRestarter");

    restartGroup.MapPost(string.Empty, RestartEndpoints.Restart);
    restartGroup.MapGet("/{namespace}/{name}", RestartEndpoints.DeprecatedRestart)
        .WithOpenApi(operation =>
        {
            operation.Deprecated = true;
            return operation;
        });

    await app.RunAsync();
}

/// <summary>
///   Program entrypoint.
/// </summary>
[SuppressMessage("Major Code Smell", "S1118:Utility classes should not have public constructors", Justification = "Stub class required in order to allow testing")]
public partial class Program;
