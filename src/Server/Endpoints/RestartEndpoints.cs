﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using PommaLabs.K8sDeploymentRestarter.Models.DataTransferObjects;
using PommaLabs.K8sDeploymentRestarter.Models.Enumerations;
using PommaLabs.K8sDeploymentRestarter.Models.Exceptions;
using PommaLabs.K8sDeploymentRestarter.Services.Restart;

namespace PommaLabs.K8sDeploymentRestarter.Server.Endpoints;

public static class RestartEndpoints
{
    /// <summary>
    ///   Schedules the restart of given deployment.
    /// </summary>
    /// <param name="deployment">Deployment to be restarted.</param>
    /// <param name="restartService">Restart service.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    public static Task<Results<Ok, UnprocessableEntity<ProblemDetails>, NotFound<ProblemDetails>>> Restart(
        Deployment deployment,
        [FromServices] IRestartService restartService,
        CancellationToken cancellationToken)
    {
        return RestartInternalAsync(restartService, deployment, cancellationToken);
    }

    /// <summary>
    ///   Schedules the restart of given deployment.
    /// </summary>
    /// <param name="namespace">Namespace to which the deployment belongs.</param>
    /// <param name="name">Deployment name.</param>
    /// <param name="restartService">Restart service.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <remarks>
    ///   The usage of the `POST` endpoint is recommended.
    /// </remarks>
    public static Task<Results<Ok, UnprocessableEntity<ProblemDetails>, NotFound<ProblemDetails>>> DeprecatedRestart(
        [FromRoute(Name = "namespace")] string @namespace,
        [FromRoute] string name,
        [FromServices] IRestartService restartService,
        CancellationToken cancellationToken)
    {
        var deployment = new Deployment { Namespace = @namespace, Name = name };
        return RestartInternalAsync(restartService, deployment, cancellationToken);
    }

    private static async Task<Results<Ok, UnprocessableEntity<ProblemDetails>, NotFound<ProblemDetails>>> RestartInternalAsync(
        IRestartService restartService, Deployment deployment, CancellationToken cancellationToken)
    {
        try
        {
            await restartService.RestartAsync(deployment, cancellationToken);
            return TypedResults.Ok();
        }
        catch (RestartException ex) when (ex.Reason == RestartErrorReason.MissingPermissions)
        {
            return TypedResults.UnprocessableEntity(new ProblemDetails
            {
                Title = ex.Message,
                Status = StatusCodes.Status422UnprocessableEntity,
            });
        }
        catch (RestartException ex) when (ex.Reason == RestartErrorReason.DeploymentNotFound)
        {
            return TypedResults.NotFound(new ProblemDetails
            {
                Title = ex.Message,
                Status = StatusCodes.Status404NotFound,
            });
        }
    }
}
