﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using PommaLabs.K8sDeploymentRestarter.Models.DataTransferObjects;
using PommaLabs.Salatino;
using PommaLabs.Salatino.Models.DataTransferObjects;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace PommaLabs.K8sDeploymentRestarter.Server.ConfigureOptions;

internal sealed class ConfigureSwaggerGenOptions(AppVersion appVersion) : IConfigureOptions<SwaggerGenOptions>
{
    [SuppressMessage("Minor Code Smell", "S1075:URIs should not be hardcoded", Justification = "External URL of AGPLv3+ license")]
    public void Configure(SwaggerGenOptions options)
    {
        options.SwaggerDoc("schema", new OpenApiInfo
        {
            Title = "Kubernetes Deployment Restarter",
            Version = appVersion.Version,
            Description = """
Kubernetes Deployment Restarter web service exposes endpoints to restart Kubernetes deployments.

Some useful links:
- [Source code](https://gitlab.com/pommalabs/k8s-deployment-restarter)
""",
            Contact = new OpenApiContact
            {
                Name = "PommaLabs Team",
                Email = "hello@pommalabs.xyz"
            },
            License = new OpenApiLicense
            {
                Name = "MIT",
                Url = new Uri("https://opensource.org/licenses/MIT")
            }
        });

        options.IncludeXmlComments(typeof(Program).Assembly);
        options.IncludeXmlComments(typeof(Deployment).Assembly);

        options.AddSecurityDefinition(Constants.ApiKeyAuthenticationScheme, new OpenApiSecurityScheme
        {
            Description = $"API key sent as \"{Constants.ApiKeyHeaderName}\" header.",
            Type = SecuritySchemeType.ApiKey,
            In = ParameterLocation.Header,
            Name = Constants.ApiKeyHeaderName
        });

        options.AddSecurityRequirement(Constants.ApiKeyAuthenticationScheme);
    }
}
